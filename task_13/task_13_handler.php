<?php
    session_start();
    
    $pdo = new PDO('mysql:host=localhost;dbname=marlin', "root", ""); 
    
    $sql = "UPDATE counter SET number = number + 1";
    $statement = $pdo->prepare($sql);
    $statement->execute();

    $sql = "SELECT * FROM counter WHERE number";
    $statement = $pdo->prepare($sql);
    $statement->execute();
    $task = $statement->fetch(PDO::FETCH_ASSOC);

    $_SESSION['counter'] = $task['number'];
    
    header("Location: task_13.php");
    
?>
