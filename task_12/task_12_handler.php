<?php
    session_start();
    
    $text = $_POST['text'];
    
    
    $pdo = new PDO('mysql:host=localhost;dbname=marlin', "root", ""); 
    
    $sql = "INSERT INTO task_12 (text) VALUES ( :text)";
    $statement = $pdo->prepare($sql);
    $statement->execute(['text' => $text]);

    $sql = "SELECT * FROM task_12 WHERE text=:text";
    $statement = $pdo->prepare($sql);
    $statement->execute(['text' => $text]);
    $task = $statement->fetch(PDO::FETCH_ASSOC);


    $message = $task['text'];
    $_SESSION['message'] = $message;
    
    header("Location: task_12.php");
    
?>
