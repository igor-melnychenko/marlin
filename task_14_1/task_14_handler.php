<?php

    session_start();

    $email = $_POST['email'];
    $password = $_POST['password'];

    $pdo = new PDO('mysql:host=localhost;dbname=marlin', "root", "");

    $sql = "SELECT * FROM auth WHERE email=:email";
    $statement = $pdo->prepare($sql);
    $statement->execute(['email' => $email]);
    $task = $statement->fetchAll(PDO::FETCH_ASSOC);

    if(!empty($task) && password_verify($password, $task[0]['password'])) {
        $message = "Hello, " . $task[0]['email'];
        $_SESSION['success'] = $message;
        header("Location: task_14_1.php");
        exit();
    }

    $message = "Неверный логин или пароль";
    $_SESSION['danger'] = $message;
    header("Location: task_14.php");
    exit();

?>
