<?php
    session_start();
    
    $email = $_POST['email'];
    $password = $_POST['password'];
    $password = password_hash($password, PASSWORD_DEFAULT);
    
    
    $pdo = new PDO('mysql:host=localhost;dbname=marlin', "root", ""); 
    
    $sql = "SELECT * FROM task_11 WHERE email=:email";
    $statement = $pdo->prepare($sql);
    $statement->execute(['email' => $email]);
    $task = $statement->fetch(PDO::FETCH_ASSOC);
    

    if(!empty($task)) {
        $message = "Введенная запись уже существует в баззе данных";
        $_SESSION['danger'] = $message;
        
        header("Location: task_11.php");
        exit;
    }
    
    $sql = "INSERT INTO task_11 (email, password) VALUES ( :email, :password)";
    $statement = $pdo->prepare($sql);
    $statement->execute(['email' => $email, 'password' => $password]);
    $message = "Вы добавили новую запись в базу";
    $_SESSION['success'] = $message;
    
    header("Location: task_11.php");
    
?>
