<?php
    session_start();
    
    $text = $_POST['text'];
    
    $pdo = new PDO('mysql:host=localhost;dbname=marlin', "root", "");
    
    $sql = "SELECT * FROM task_9 WHERE text=:text";
    $statement = $pdo->prepare($sql);
    $statement->execute(['text' => $text]);
    $task = $statement->fetch(PDO::FETCH_ASSOC);
    
    if(!empty($task)) {
        $message = "Введенная запись уже существует в баззе данных";
        $_SESSION['message'] = $message;
        
        header("Location: task_10.php");
        exit;
    }
    
    $sql = "INSERT INTO task_9 (text) VALUES (:text)";
    $statement = $pdo->prepare($sql);
    $statement->execute(['text' => $text]);
    
    header("Location: task_10.php");
?>
