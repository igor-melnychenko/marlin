<?php
session_start();
$pdo = new PDO('mysql:host=localhost;dbname=marlin', "root", "");


$uploaddir = 'uploads/';

$img_name = $_FILES['my_image']['name'];
$img_ex = pathinfo($img_name, PATHINFO_EXTENSION);
$img_ex_lc = strtolower($img_ex);
$new_img_name = uniqid("IMG-", true).'.'.$img_ex_lc;

$uploadfile = $uploaddir . $new_img_name;

if (move_uploaded_file($_FILES['my_image']['tmp_name'], $uploadfile)) {
    $sql = "INSERT INTO images (img_url) VALUES ( :new_img_name)";
    $statement = $pdo->prepare($sql);
    $statement->execute(['new_img_name' => $new_img_name]);
    // var_export($statement); die();
} else {
    echo "Возможная атака с помощью файловой загрузки!\n";
}

$sql = "SELECT * FROM images";
$statement = $pdo->prepare($sql);
$statement->execute();
$task = $statement->fetchAll(PDO::FETCH_ASSOC);
$_SESSION['items'] = $task;

header('Location: task_15.php')


?>
